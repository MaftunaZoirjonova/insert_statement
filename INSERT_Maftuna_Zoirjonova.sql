-- Task 1. Choose one of your favorite films and add it to the "film" table. Fill in rental rates with 4.99 and rental durations with 2 weeks.
-- 2 weeks = 14 days for the column rental_duration
INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration,
                  rental_rate, length, replacement_cost, rating, special_features, fulltext)
SELECT UPPER('Red notice'),
        'An Action-Packed Treasure Hunt Uniting an FBI Agent, Art Thief',
        2021,
        l.language_id,
        l.language_id,
        14,
        4.99,
        118,
        24.99,
        'PG-13',
       '{Trailers}',
        'null'
FROM language l
WHERE UPPER(l.name) = 'ENGLISH';

-- Task 2. Add the actors who play leading roles in your favorite film to the "actor" and "film_actor" tables (three or more actors in total).

INSERT INTO actor (first_name, last_name)
VALUES ('Gal', 'Gadot'),
       ('Ryan', 'Reynolds'),
       ('Dwayne', 'Johnson'),
       ('Ritu', 'Arya');

INSERT INTO film_actor (actor_id, film_id)
SELECT
    a.actor_id,
    f.film_id
FROM actor a
    JOIN film f ON UPPER(f.title) = UPPER('Red Notice')
WHERE a.first_name = 'Gal' AND a.last_name = 'Gadot'
   OR a.first_name = 'Ryan' AND a.last_name = 'Reynolds'
   OR a.first_name = 'Dwayne' AND a.last_name = 'Johnson'
   OR a.first_name = 'Ritu' AND a.last_name = 'Arya';

-- Task 3. Add your favorite movies to any store's inventory.

INSERT INTO inventory (film_id, store_id)
SELECT
    f.film_id, s.store_id
FROM film f
CROSS JOIN (
    SELECT store_id
    FROM store
    ORDER BY store_id
    LIMIT 1
) s
WHERE UPPER(f.title) IN (UPPER('Red Notice'), UPPER('Ali Forever'));

